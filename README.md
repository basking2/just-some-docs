# Just Some Docs

## Deployment

The default administrative account is admin with the password admin.

## API

### PUT /user/file

Write a file and set some custom properties on it.
Currently there are only two properties. The `public` property allows
any unauthenticated user to read it. The `content_type` property
will set the type of the file when it is returned. The `content_type`
property may alternately be set by the `Content-Type` request header.

```
PUT /user/file
Authorization: Basic ....
X-JustSomeDocs-Props: public=true, content_type=text/plain
```

### GET /user/file

```
GET /user/file
Authorization: Basic ....
```

### GET /user/file?prop

Return the properties. The value `prop` may be assigned anything or be
left undefined. Its presense is enough to return the JSON properties
for the file.

```
GET /user/file?props
Authorization: Basic ....
```


## Debugging

Interact using `curl` with user1 with password user1.

    echo test | curl  -T - -v -H Authorization:Basic\ dXNlcjE6dXNlcjE= -k https://localhost:8088/api/user1/testfile
    echo test | curl  -T - -v -H "X-JustSomeDocs-Props: a=b, c = d , public=1" -H Authorization:Basic\ dXNlcjE6dXNlcjE= -k https://localhost:8088/api/user1/testfile
    echo test | curl  -T - -v -H "Content-Type: text/plain" -H "X-JustSomeDocs-Props: a=b, c = d , public=1" -H Authorization:Basic\ dXNlcjE6dXNlcjE= -k https://localhost:8088/api/user1/testfile
    curl  -v -H Authorization:Basic\ dXNlcjE6dXNlcjE= -k https://localhost:8088/api/user1/testfile
