#include "http_error.hpp"

#include "http_common.hpp"

boost::beast::http::response<boost::beast::http::string_body> auth_error(
    const boost::beast::http::request<boost::beast::http::string_body>& req0
) {
    boost::beast::http::response<boost::beast::http::string_body> res{boost::beast::http::status::unauthorized, req0.version()};
    res.set(boost::beast::http::field::server, API_SERVER_VERSION);
    res.set(boost::beast::http::field::content_type, "text/html");
    if (req0["X-JustSomeDocs-UI"] == std::string()) {
      // When the Client UI is not requesting files, don't return a WWW-Authenticate header.
      // The UI will handle it when it sees the 401 response code.
      res.set(boost::beast::http::field::www_authenticate, "Basic realm=\"Just Some Docs\"");
    }

    res.keep_alive(req0.keep_alive());
    res.body() = "Authorization needed.";
    res.prepare_payload();
    return res;
}

boost::beast::http::response<boost::beast::http::string_body> wrong_user_write
(
    const boost::beast::http::request<boost::beast::http::string_body>& req0,
    const std::string& user1, 
    const std::string& user2
)
{
    boost::beast::http::response<boost::beast::http::string_body> res{boost::beast::http::status::unauthorized, req0.version()};
    res.set(boost::beast::http::field::server, API_SERVER_VERSION);
    res.set(boost::beast::http::field::content_type, "text/html");
    res.keep_alive(req0.keep_alive());
    res.body() = std::string("User ")+user1+" may not write files of "+user2+".";
    res.prepare_payload();
    return res;
};

boost::beast::http::response<boost::beast::http::string_body> wrong_user_read(
    const boost::beast::http::request<boost::beast::http::string_body>& req0,
    const std::string& user1,
    const std::string& user2
)
{
    boost::beast::http::response<boost::beast::http::string_body> res{boost::beast::http::status::unauthorized, req0.version()};
    res.set(boost::beast::http::field::server, API_SERVER_VERSION);
    res.set(boost::beast::http::field::content_type, "text/html");
    res.keep_alive(req0.keep_alive());
    res.body() = std::string("User ")+user1+" may not read files of "+user2+".";
    res.prepare_payload();
    return res;
};

boost::beast::http::response<boost::beast::http::string_body> not_found(
    const boost::beast::http::request<boost::beast::http::string_body>& req0
) {
    boost::beast::http::response<boost::beast::http::string_body> res{boost::beast::http::status::not_found, req0.version()};
    res.set(boost::beast::http::field::server, API_SERVER_VERSION);
    res.set(boost::beast::http::field::content_type, "text/html");
    res.keep_alive(req0.keep_alive());
    res.body() = "File not found.";
    res.prepare_payload();
    return res;
};
