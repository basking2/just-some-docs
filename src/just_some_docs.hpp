#ifndef __JUST_SOME_DOCS_HPP
#define __JUST_SOME_DOCS_HPP

#include "data_store.hpp"

/**
 * This is the application layer. All business logic and configurations
 * should go in here and be passed around.
 */
class just_some_docs {
    
    data_store& ds_;

    public:
    explicit just_some_docs(data_store& ds);

    data_store& get_data_store();
    const data_store& get_data_store() const;
};

#endif
