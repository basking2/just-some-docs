#ifndef __HTTP_MIME_HPP
#define __HTTP_MIME_HPP

#include <boost/beast.hpp>

boost::beast::string_view
mime_type(boost::beast::string_view path);

#endif
