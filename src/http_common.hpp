#ifndef __HTTP_COMMON_HPP
#define __HTTP_COMMON_HPP
#include "version.hpp"

#define API_SERVER_VERSION "JustSomeDocsApi " JUST_SOME_DOCS_VERSION
#define SERVER_VERSION "JustSomeDocsApi " JUST_SOME_DOCS_VERSION

#endif
