#include "just_some_docs.hpp"

#include "data_store.hpp"

just_some_docs::just_some_docs(data_store& ds): ds_(ds) {

}

data_store& just_some_docs::get_data_store() {
    return ds_;
}

const data_store& just_some_docs::get_data_store() const {
    return ds_;
}
