#include "http.hpp"

#include "data_store.hpp"
#include "just_some_docs.hpp"

#include <openssl/crypto.h>

#include <boost/log/trivial.hpp>

int main(int args, char** argv) {

    // Initialize OpenSSL here before we start using any threads.
    OPENSSL_init_ssl(0, 0);

    data_store ds("build/data");

    just_some_docs app(ds);

    BOOST_LOG_TRIVIAL(info) << "Starting Just Some Docs server.";

    HttpServer httpServer = HttpServer("build/ssl.cert", "build/ssl.key", app);

    return 0;
}
