#ifndef __HTTP_ERROR_HPP
#define __HTTP_ERROR_HPP

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

boost::beast::http::response<boost::beast::http::string_body> auth_error(
    const boost::beast::http::request<boost::beast::http::string_body>& req0
);


boost::beast::http::response<boost::beast::http::string_body> wrong_user_read(
    const boost::beast::http::request<boost::beast::http::string_body>& req0,
    const std::string& user1,
    const std::string& user2
);

boost::beast::http::response<boost::beast::http::string_body> wrong_user_write(
    const boost::beast::http::request<boost::beast::http::string_body>& req0,
    const std::string& user1, 
    const std::string& user2
);

boost::beast::http::response<boost::beast::http::string_body> not_found(
    const boost::beast::http::request<boost::beast::http::string_body>& req0
);

#endif
