#ifndef __HTTP_API_HPP
#define __HTTP_API_HPP

// vim: set et  sw=4 : 

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/core/flat_buffer.hpp>
#include <boost/log/trivial.hpp>

#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#include <boost/property_tree/json_parser.hpp>
#undef BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <boost/regex.hpp>
#include <vector>

#include "version.hpp"
#include "just_some_docs.hpp"
#include "util.hpp"
#include "http_mime.hpp"
#include "http_common.hpp"
#include "http_error.hpp"

namespace {

  const boost::regex authorization_re("\\s*Basic\\s+(\\S+)", boost::regex::perl|boost::regex::icase);
  const boost::regex header_split_re("\\s*([^:]+):(\\S*)", boost::regex::perl|boost::regex::icase);

  const boost::regex equals_split_re("\\s*([^=\\s]+)\\s*=\\s*(\\S*)\\s*,?", boost::regex::perl|boost::regex::icase);

  const boost::regex query_args_re("[\\?&]([^=]+)(?:=([^&]+))?", boost::regex::perl|boost::regex::icase);

  bool split_on_colon(const std::string& in, std::string& out1, std::string& out2) {
    boost::smatch m;

    if (boost::regex_match(in, m, header_split_re)) {
      out1 = m[1];
      out2 = m[2];
      return true;
    }

    return false;
  }

  void get_query_parameters(
    const boost::beast::string_view& target,
    boost::property_tree::ptree& properties
  ) {
    for (
      boost::cregex_iterator itr(target.begin(), target.end(), query_args_re);
      itr != boost::cregex_iterator();
      ++itr)
    {
      std::string key((*itr)[1].first, (*itr)[1].second);
      std::string value((*itr)[1].first, (*itr)[1].second);
      properties.put(key, value);
    }
  }

  /**
   * Split and parse the header X-JustSomeDocs-Props: p1=v1, p2=v2 into the
   * property_tree passed in.
   * 
   * @param[in] req The HTTP request.
   * @param[out] properties The propeties parsed from the header are put into this.
   */
  template<class Body, class Allocator>
  void get_request_properties(
    boost::beast::http::request<Body, boost::beast::http::basic_fields<Allocator>>& req,
    boost::property_tree::ptree& properties
  ) {
    
    for (
      auto props_itr = req.find("X-JustSomeDocs-Props");
      props_itr != req.end();
      ++props_itr)
    {
      const std::string props = props_itr->value().to_string();

      for (int pos = props.find_first_not_of(", \t"); pos >= 0; ) {
          const int nxt = props.find_first_of(",", pos+1);
          std::string prop;

          if (nxt < 0) {
              prop = props.substr(pos);
              pos = nxt;
          } else {
              prop = props.substr(pos, nxt - pos);
              pos = nxt+1;
          }

          boost::smatch m;

          if (boost::regex_match(prop, m, equals_split_re)) {
              const std::string m1 = m[1];
              properties.put(m1, m[2]);
          } else {
              BOOST_LOG_TRIVIAL(warning) << "Could not parse header "<<prop;
          }

      }
    }
  }

  template<
    class Body,
    class Allocator
  >
  void
  access_log(
      boost::beast::http::request<Body, boost::beast::http::basic_fields<Allocator>>& req,
      const std::string& user,
      const std::string& message,
      const std::string& owner,
      const std::string& filename
  )
  {
    BOOST_LOG_TRIVIAL(info) << user << " " << message << " for user "<< owner << " file "<< filename;
  }
}

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_serve_file(
    const just_some_docs& app,
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req,
    const std::string& user,
    const std::string& filename,
    const boost::property_tree::ptree& properties
);

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_list_dir(
    const just_some_docs& app,
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req,
    const std::string& user,
    const std::string& filename,
    const boost::property_tree::ptree& properties
);

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_serve_json(
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req,
    const boost::property_tree::ptree& properties
);

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_set_user_account_meta(
    just_some_docs& app,
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req,
    const bool isadmin,
    const bool isself,
    const std::string& user
);

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_list_users(
    const just_some_docs& app,
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req
);

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_create_user_account(
    just_some_docs& app,
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req,
    const std::string& user
);

template<
  class Body,
  class Allocator,
  class Send
>
void
handle_api_request(
    just_some_docs& app,
    boost::beast::http::request<Body, boost::beast::http::basic_fields<Allocator>>& req0,
    Send& send)
{
    // const boost::beast::string_view authheader64 = req["Authorization"];
    const std::string authheader64 = req0["Authorization"].to_string();
    boost::smatch mresult;

    std::vector<std::string> path_elements;
    boost::filesystem::path filename = "";
    bool dir_listing;
    {
      // This block defines path_elements and filename.

      const boost::beast::string_view& target = req0.target();
      for (int pos = target.find_first_not_of("/?", 4); pos >= 0;) {
        const int next = target.find_first_of("/?", pos);

        path_elements.push_back(std::string(target.substr(pos, next - pos)));

        if (target[next] == '?') {
          break;
        }

        pos = target.find_first_not_of("/?", next);
      }

      dir_listing = target.ends_with("/");

      // Corner case. For root directory listings, add "." as the file name.
      if (path_elements.size() < 2 && dir_listing) {
        path_elements.push_back(".");
      }

      for (int i = 1; i < path_elements.size(); ++i) {
        filename = filename / path_elements[i];
      }
    }

    const std::string filename_str = filename.string();
    boost::property_tree::ptree properties;

    const std::string& store_user = (path_elements.size() == 0) ? std::string() : path_elements[0];

    app.get_data_store().read_file_properties(store_user, filename_str, properties);

    if (boost::regex_match(authheader64, mresult, authorization_re)) {

      std::vector<unsigned char> v;
      base64decode(mresult[1], v);
      std::string user;
      std::string pass;

      if (split_on_colon(std::string(reinterpret_cast<char*>(&v[0])), user, pass)) {
        if (app.get_data_store().check_password(user, pass)) {

          if (req0.method() == boost::beast::http::verb::post) {

            if (filename_str.empty() && !dir_listing) {
              boost::property_tree::ptree acct;
              app.get_data_store().read_account(user, acct);

              const bool isadmin = app.get_data_store().has_role("admin", acct);
              const bool isself = user == store_user;
              if (isadmin || isself) {
                access_log(req0, user, "writing properties", store_user, filename_str);
                return send(std::move(http_api_set_user_account_meta(app, req0, isadmin, isself, store_user)));
              } else {
                return send(std::move(auth_error(req0)));
              }
            }

            boost::beast::http::response<boost::beast::http::string_body> res{boost::beast::http::status::ok, req0.version()};
            res.set(boost::beast::http::field::server, API_SERVER_VERSION);
            res.set(boost::beast::http::field::content_type, "text/plain");
            res.keep_alive(req0.keep_alive());
            res.body() = "Not implemented.";
            return send(std::move(res));
          }

          if (req0.method() == boost::beast::http::verb::put) {
            if (filename_str.empty() && ! dir_listing) {
              // This is a situation of creating a user.
              boost::property_tree::ptree account;
              app.get_data_store().read_account(user, account);
              if (app.get_data_store().has_role("admin", account)) {
                access_log(req0, user, "creating user", store_user, filename_str);
                return send(std::move(http_api_create_user_account(app, req0, store_user)));

              } else {
                access_log(req0, user, "failed creating user", store_user, filename_str);
                return send(std::move(wrong_user_write(req0, user, store_user)));
              }
            }

            if (user != store_user) {
              // This is a situation of a user trying to write someone else's files.
              access_log(req0, user, "failed writing", store_user, filename_str);
              return send(std::move(wrong_user_write(req0, user, store_user)));
            }

            access_log(req0, user, "writing", store_user, filename_str);

            // Extract properties for file puts.
            boost::property_tree::ptree properties = boost::property_tree::ptree();

            // Set the content_type property from the Content-Type header, if present.
            const auto content_type = req0.find(boost::beast::http::field::content_type);
            if (content_type != req0.end()) {
              properties.put("content_type", content_type->value().to_string());
            }
            
            get_request_properties(req0, properties);

            boost::beast::http::response<boost::beast::http::string_body> res{boost::beast::http::status::ok, req0.version()};
            res.set(boost::beast::http::field::server, API_SERVER_VERSION);
            res.set(boost::beast::http::field::content_type, mime_type(filename_str));
            res.keep_alive(req0.keep_alive());

            // FIXME - consider chunked.
            boost::beast::http::request<boost::beast::http::string_body> req{req0};

            app.get_data_store().write_file(store_user, filename_str, req.body(), properties);
            res.prepare_payload();
            return send(std::move(res));

          }
          
          if (req0.method() == boost::beast::http::verb::get) {

            if (store_user.empty()) {
              boost::property_tree::ptree acct;
              app.get_data_store().read_account(user, acct);
              if (app.get_data_store().has_role("admin", acct)) {
                return send(std::move(http_api_list_users(app, req0)));
              }
              else {
                return send(std::move(auth_error(req0)));
              }
            }

            if (filename_str.empty() && !dir_listing) {
              boost::property_tree::ptree acct;
              app.get_data_store().read_account(user, acct);
              if (user == store_user) {
                access_log(req0, user, "reading properties", store_user, filename_str);
                return send(std::move(http_api_serve_json(req0, acct)));
              } else if (app.get_data_store().has_role("admin", acct)) {
                boost::property_tree::ptree store_acct;
                app.get_data_store().read_account(store_user, store_acct);
                access_log(req0, user, "reading properties", store_user, filename_str);
                return send(std::move(http_api_serve_json(req0, store_acct)));
              } else {
                return send(std::move(auth_error(req0)));
              }
            }

            if (user != store_user) {
              // If the user is not the owner, check if the file is public.
              if (properties.find("public") == properties.not_found()) {
                access_log(req0, user, "falied reading", store_user, filename_str);
                return send(wrong_user_read(req0, user, store_user));
              }
            }

            access_log(req0, user, "reading", store_user, filename_str);
            if (dir_listing) {
              return send(std::move(http_api_list_dir(app, req0, store_user, filename_str, properties)));
            } else {
              return send(std::move(http_api_serve_file(app, req0, store_user, filename_str, properties)));
            }
          }

          // FIXME - put a default error
          return send(std::move(auth_error(req0)));

        } else {
          BOOST_LOG_TRIVIAL(info) << "bad password";
          return send(std::move(auth_error(req0)));
        }
      } else {
          BOOST_LOG_TRIVIAL(info) << "bad auth header";
          return send(std::move(auth_error(req0)));
      }
    } else {
      // Try serving anonymously.
      if (properties.find("public") == properties.not_found()) {
        access_log(req0, "[unauthenticated]", "failed reading", store_user, filename_str);
        return send(std::move(auth_error(req0)));
      }

      access_log(req0, "[unauthenticated]", "reading", store_user, filename_str);

      if (dir_listing) {
        return send(std::move(http_api_list_dir(app, req0, store_user, filename_str, properties)));
      }
      else {
        return send(std::move(http_api_serve_file(app, req0, store_user, filename_str, properties)));
      }
    }
}

#endif
