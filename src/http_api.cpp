#include "http_api.hpp"

#include <boost/optional.hpp>

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_list_dir(
    const just_some_docs& app,
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req,
    const std::string& user,
    const std::string& filename,
    const boost::property_tree::ptree& properties
) {
  // Setup the basic response.
  boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>&& res{boost::beast::http::status::ok, req.version()};
  res.set(boost::beast::http::field::server, API_SERVER_VERSION);
  res.keep_alive(req.keep_alive());

  if (app.get_data_store().exists(user, filename)) {

    boost::property_tree::ptree query_arguments;

    get_query_parameters(req.target(), query_arguments);

    res.set(boost::beast::http::field::content_type, "application/justsomedocs-dirlist");
    std::vector<data_store_directory_listing> lst;
    app.get_data_store().list(user, filename, lst);
    for (auto itr = lst.begin(); itr != lst.end(); ++itr) {
      res.body() += itr->string() + "\n";
    }

    res.prepare_payload();
    return res;
  } else {
    res.set(boost::beast::http::field::content_type, "text/html");
    res.body() = "File not found.";
    res.prepare_payload();
    return res;
  }
}

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_serve_json(
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req,
    const boost::property_tree::ptree& properties
) {
  boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>&& res{boost::beast::http::status::ok, req.version()};
  res.keep_alive(req.keep_alive());
  res.set(boost::beast::http::field::server, API_SERVER_VERSION);
  res.set(boost::beast::http::field::content_type, "application/json");
  std::ostringstream json;
  boost::property_tree::json_parser::write_json(json, properties, true);
  res.body() = json.str();
  res.prepare_payload();
  return res;
}

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_list_users(
    const just_some_docs& app,
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req
) {
  boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>&& res{boost::beast::http::status::ok, req.version()};
  res.keep_alive(req.keep_alive());
  res.set(boost::beast::http::field::server, API_SERVER_VERSION);
  res.set(boost::beast::http::field::content_type, "application/justsomedocs-dirlist");

  std::vector<std::string> users;
  app.get_data_store().list_users(users);

  for (auto itr = users.begin(); itr != users.end(); ++itr) {
    res.body() += itr->data() + std::string("\n");
  }

  res.prepare_payload();
  return res;
}


boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_serve_file(
    const just_some_docs& app,
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req,
    const std::string& user,
    const std::string& filename,
    const boost::property_tree::ptree& properties
) {

  // Setup the basic response.
  boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>&& res{boost::beast::http::status::ok, req.version()};
  res.set(boost::beast::http::field::server, API_SERVER_VERSION);
  res.keep_alive(req.keep_alive());

  if (app.get_data_store().exists(user, filename)) {

    boost::property_tree::ptree query_arguments;

    get_query_parameters(req.target(), query_arguments);

    if (query_arguments.find("props") != query_arguments.not_found()) {
      // boost::property_tree::ptree meta;
      // app.get_data_store().read_file_properties(user, filename, meta);
      res.set(boost::beast::http::field::content_type, "application/json");
      std::ostringstream json;
      boost::property_tree::json_parser::write_json(json, properties, true);
      res.body() = json.str();
      return res;
    }

    const auto content_type = properties.find("content_type");
    if (content_type == properties.not_found()) {
      res.set(boost::beast::http::field::content_type, mime_type(filename));
    } else {
      res.set(boost::beast::http::field::content_type, content_type->second.data());
    }

    app.get_data_store().read_file(user, filename, res.body());

    res.prepare_payload();

    return res;
  } else {
    res.set(boost::beast::http::field::content_type, "text/html");
    res.result(boost::beast::http::status::not_found);
    res.body() = "File not found.";
    res.prepare_payload();
    return res;
  }
}

void merge_ptrees(
  boost::property_tree::ptree& to,
  const boost::property_tree::ptree& from
) {
  if (from.empty()) {
    to.put_value<std::string>(from.get_value<std::string>());
  }
  else {
    for (
      auto itr = from.begin();
      itr != from.end();
      ++itr
    ) {
      auto subtree = to.get_child_optional(itr->first);
      if (subtree) {
        merge_ptrees(*subtree, itr->second);
      } else {
        merge_ptrees(to.put_child(itr->first, boost::property_tree::ptree()), itr->second);
      }
    }
  }
}

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_set_user_account_meta(
    just_some_docs& app,
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req,
    const bool isadmin,
    const bool isself,
    const std::string& user
) {

  boost::property_tree::ptree meta;
  boost::property_tree::ptree store_meta;

  // Load body.
  std::istringstream iss(req.body());
  boost::property_tree::json_parser::read_json(iss, meta);

  boost::optional<std::string> pwopt = meta.get_optional<std::string>("password.plain");
  if (pwopt) {
    if (isadmin || isself) {
      app.get_data_store().set_password(user, *pwopt);
    }
  }

  // Remove permissions unless the user is an admin.
  if (!isadmin) {
    meta.erase("permissions");
  }

  // Remove the password's subtree.
  meta.erase("password");

  // Get the user's account information.
  app.get_data_store().read_account(user, store_meta);

  // Merge them.
  merge_ptrees(store_meta, meta);

  // Update (write) the account information.
  app.get_data_store().write_account(user, store_meta);

  // Clear and reload the user account to serve to the user.
  store_meta.clear();
  app.get_data_store().read_account(user, store_meta);

  boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>&& res{boost::beast::http::status::ok, req.version()};
  res.set(boost::beast::http::field::server, API_SERVER_VERSION);
  res.keep_alive(req.keep_alive());
  res.set(boost::beast::http::field::content_type, "application/json");
  std::ostringstream oss;
  boost::property_tree::json_parser::write_json(oss, store_meta, true);
  res.body() = oss.str();
  res.prepare_payload();
  return res;
}

boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>
http_api_create_user_account(
    just_some_docs& app,
    const boost::beast::http::request<boost::beast::http::string_body, boost::beast::http::fields>& req,
    const std::string& user
) {

  boost::property_tree::ptree meta;
  
  // Load body.
  std::istringstream iss(req.body());
  boost::property_tree::json_parser::read_json(iss, meta);

  boost::beast::http::response<boost::beast::http::string_body, boost::beast::http::fields>&& res{boost::beast::http::status::ok, req.version()};
  res.set(boost::beast::http::field::server, API_SERVER_VERSION);
  res.keep_alive(req.keep_alive());

  boost::optional<std::string> pwopt = meta.get_optional<std::string>("password.plain");
  if (!pwopt) {
    BOOST_LOG_TRIVIAL(error) << "No password given. Not making a new account with no password.";
    res.result(boost::beast::http::status::bad_request);
    res.set(boost::beast::http::field::content_type, "text/plain");
    res.body() = "Refusing to create a new user account without a password set.";
    return res;
  }

  const std::list<std::string> permissions;
  app.get_data_store().create_account(user, *pwopt, permissions);

  // Remove password from meta before writing it to the account.
  meta.erase("password");
  app.get_data_store().write_account(user, meta);
  

  // Clear and reload the user account to serve to the user.
  meta.clear();
  app.get_data_store().read_account(user, meta);

  res.set(boost::beast::http::field::content_type, "application/json");
  std::ostringstream oss;
  boost::property_tree::json_parser::write_json(oss, meta, true);
  res.body() = oss.str();
  res.prepare_payload();
  return res;
}
