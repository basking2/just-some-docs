#ifndef __UTIL_HPP
#define __UTIL_HPP

#include <string>
#include <vector>
#include <boost/property_tree/ptree.hpp>

/**
 * Return a C string in the given vector. NOTE: The out vector may be longer
 * then the string it contains.
 * 
 * @param[in] in Input.
 * @param[out] out Output vector.
 */
void base64decode(const std::string& in, std::vector<unsigned char>& out);

void base64encode(const std::vector<unsigned char>& in, std::vector<unsigned char>& out);

#endif