#include "data_store.hpp"

#include "util.hpp"

#include <openssl/evp.h>
#include <openssl/rand.h>

#include <iostream>
#include <fstream>
#include <list>
#include <vector>

#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>

#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#include <boost/property_tree/json_parser.hpp>
#undef BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <boost/log/trivial.hpp>

namespace fs = boost::filesystem;
namespace pt = boost::property_tree;

namespace {

    const int PW_HASH_LEN = 256;
    const int SALT_LEN = 256;

    void newsalt(std::string& out) {
        std::vector<unsigned char> buf(SALT_LEN);

        int rc = RAND_bytes(&buf[0], SALT_LEN);

        if (rc != 1) {
            // FIXME - better errors.
            throw std::runtime_error("Can't get random salt.");
        }

        std::vector<unsigned char> digest;

        base64encode(buf, digest);

        out = reinterpret_cast<char*>(&digest[0]);
    }

    void hash_password(
        std::string& hashout, 
        const std::string& pass,
        const std::string& salt64
    ) {

        // Decode the salt.
        std::vector<unsigned char> salt;

        base64decode(salt64, salt);

        const EVP_MD *digest = EVP_sha256();

        std::vector<unsigned char> c(PW_HASH_LEN);

        int ec = PKCS5_PBKDF2_HMAC(
            pass.c_str(), pass.length(),
            &salt[0], salt.size(),
            2*10000,
            digest,
            PW_HASH_LEN, &c[0]
        );

        std::vector<unsigned char> pwhash;

        base64encode(c, pwhash);

        hashout = reinterpret_cast<char*>(&pwhash[0]);
    }
}

data_store_directory_listing::data_store_directory_listing(
    const std::string& _flags,
    const std::string& _name
): flags(_flags), name(_name) {
        
}

std::string data_store_directory_listing::string() const  {
    std::string s = std::string();
    s += flags + " " + name;
    return s;
}



data_store::data_store(const std::string& doc_root_): doc_root(doc_root_) {
    fs::create_directories(doc_root);

    const fs::path &default_admin = doc_root / "users" / "admin";

    if (!fs::exists(default_admin)) {
        std::list<std::string> permissions;
        permissions.push_back("admin");

        create_account("admin", "admin", permissions);
    }
}

void data_store::create_account(
    const std::string& username,
    const std::string& password,
    const std::list<std::string>& permissions)
{
    const fs::path d = doc_root / "users" / username;

    fs::create_directories(d);

    std::string salt64;
    newsalt(salt64);
    std::string password_hash;
    hash_password(password_hash, password, salt64);

    std::ofstream os;
    pt::ptree meta;
    meta.put("name", username);
    meta.put("password.hash", password_hash);
    meta.put("password.salt", salt64);

    BOOST_FOREACH(const std::string &s, permissions) {
        meta.put(std::string("permissions.")+s, 1);
    }

    os.open((d / "meta.json").string(), std::fstream::out);
    pt::json_parser::write_json(os, meta, true);
    os.close();
}

bool data_store::check_password(
    const std::string& username,
    const std::string& password
) {
    const fs::path meta_file = doc_root / "users" / username / "meta.json";

    if (!fs::exists(meta_file)) {
        return false;
    }

    std::ifstream ins;
    pt::ptree meta;
    ins.open(meta_file.string(), std::fstream::in);
    pt::json_parser::read_json(ins, meta);
    ins.close();

    std::string password_hash_stored = "";
    std::string password_hash_user = "";
    std::string salt64 = "";
    password_hash_stored = meta.get<std::string>("password.hash");
    salt64 = meta.get<std::string>("password.salt");

    hash_password(password_hash_user, password, salt64);

    return password_hash_user == password_hash_stored;
}

bool data_store::exists(
    const std::string& username,
    const std::string& filename
) const {

    const auto path = doc_root / "data" / username / filename;

    return boost::filesystem::exists(path);
}

void data_store::read_file(
    const std::string& username,
    const std::string& filename,
    std::string& out
) const {

    const auto path = doc_root / "data" / username / filename;

    if (boost::filesystem::exists(path)) {
        boost::filesystem::ifstream ins;
        ins.open(path, boost::filesystem::fstream::in);

        std::vector<char> buf(1024);
        while (ins) {
            ins.read(&buf[0], buf.size());
            if (ins.gcount() > 0) {
                out += std::string(&buf[0], ins.gcount());
            }
        }

        ins.close();
    } else {
        throw std::runtime_error(std::string("No such file: ")+path.string());
    }
}

void data_store::write_file(
    const std::string& username,
    const std::string& filename,
    const std::string& in,
    const boost::property_tree::ptree& properties
) {

    boost::system::error_code ec;

    const auto path = doc_root / "data" / username / filename;

    const auto prop_path = doc_root / "meta" / username / filename;


    {   // This block writes the meta file.

        // Check that the parent directory exists.
        if (!boost::filesystem::exists(prop_path.parent_path())) {
            boost::filesystem::create_directories(prop_path.parent_path(), ec);
            if (ec) {
                throw std::runtime_error(std::string("Can't create directory for ")+prop_path.string());
            }
        }
        boost::filesystem::ofstream outs;
        outs.open(prop_path, boost::filesystem::fstream::out);
        pt::json_parser::write_json(outs, properties, true);
        outs.close();
    }

    {   // This block writes the file.

        // Check that the parent directory exists.
        if (!boost::filesystem::exists(path.parent_path())) {
            boost::filesystem::create_directories(path.parent_path(), ec);
            if (ec) {
                throw std::runtime_error(std::string("Can't create directory for ")+path.string());
            }
        }

        boost::filesystem::ofstream outs;
        outs.open(path, boost::filesystem::fstream::out);

        outs << in;

        outs.close();
    }
}

void data_store::delete_file(
    const std::string& username,
    const std::string& filename
) {
    boost::system::error_code ec;

    auto path = doc_root / "data" / username / filename;

    auto prop_path = doc_root / "meta" / username / filename;

    const auto data_path = doc_root / "data" / username;

    // NOTE: This loop iterates on path, but processes prop_path in parallel.
    while (path != data_path) {
        if (boost::filesystem::exists(path)) {
            // Remove the data file.
            boost::filesystem::remove(path, ec);
        }

        if (boost::filesystem::exists(prop_path)) {
            // Remove the meta file. It is OK if this fails.
            boost::filesystem::remove(prop_path, ec);
        }

        path = path.parent_path();
        prop_path = prop_path.parent_path();
        if (boost::filesystem::directory_iterator(path, ec) != boost::filesystem::directory_iterator()) {
            // If the directory pointed to by path, now, is not empty, break.
            break;
        }
    }
}

void data_store::read_file_properties(
    const std::string& username,
    const std::string& filename,
    boost::property_tree::ptree& properties
) const {
    boost::system::error_code ec;

    auto prop_path = doc_root / "meta" / username / filename;

    if (boost::filesystem::is_regular(prop_path)) {
        std::ifstream ins = std::ifstream();
        ins.open(prop_path.string(), std::fstream::in);
        boost::property_tree::json_parser::read_json(ins, properties);
        ins.close();
    }
}

void data_store::write_file_properties(
    const std::string& username,
    const std::string& filename,
    const boost::property_tree::ptree& properties
) {
    boost::system::error_code ec;

    auto prop_path = doc_root / "meta" / username / filename;

    // Only write property file if the described file exists.
    // This allows a file to be described if its property file is removed.
    // When *creating* a file, we write the property file first to guard
    // aginst race conditions.
    if (boost::filesystem::is_regular(prop_path)) {
        std::ofstream outs = std::ofstream();
        outs.open(prop_path.string(), std::fstream::out);
        boost::property_tree::json_parser::write_json(outs, properties, true);
        outs.close();
    }
}

void data_store::list(
    const std::string& username,
    const std::string& dirname,
    std::vector<data_store_directory_listing>& listing
) const {
    auto path = doc_root / "data" / username / dirname;

    if (boost::filesystem::is_directory(path)) {
        for (boost::filesystem::directory_iterator d = boost::filesystem::directory_iterator(path); d != boost::filesystem::directory_iterator(); ++d) {
            std::string flags;
            std::string name;
            if (boost::filesystem::is_directory(*d)) {
                flags += "d";
                name = d->path().filename().string() + "/";
            } else {
                flags += "f";
                name = d->path().filename().string();
            }

            listing.push_back(data_store_directory_listing(flags, name));
        }
    }
}

bool data_store::is_directory(
    const std::string& username,
    const std::string& path
) const {

    return boost::filesystem::is_directory(doc_root / "data" / username / path);
}

bool data_store::has_role(
    const std::string& role,
    const boost::property_tree::ptree& properties
) const {
    auto opt = properties.get_child_optional("permissions");
    if (opt) {
        return static_cast<bool>(opt->get_optional<std::string>(role));
    }
    return false;
}

void data_store::read_account(
    const std::string& username,
    boost::property_tree::ptree &user
) const {
    const fs::path meta_file = doc_root / "users" / username / "meta.json";

    if (!fs::exists(meta_file)) {
        return;
    }

    std::ifstream ins;
    ins.open(meta_file.string(), std::fstream::in);
    pt::json_parser::read_json(ins, user);
    ins.close();

    // Delete each child node.
    auto pw = user.get_child_optional("password");
    if (pw) {
        pw->erase("hash");
        pw->erase("salt");
    }

    // For the moment we also clear out the entire password node.
    // In the future there may be elements of the password node we serve.
    user.erase("password");

}

void data_store::write_account(
    const std::string& username,
    const boost::property_tree::ptree &user
) {
    const fs::path meta_file = doc_root / "users" / username / "meta.json";

    if (!fs::exists(meta_file)) {
        return;
    }

    boost::property_tree::ptree prev_meta;
    boost::property_tree::ptree meta{user};

    std::ifstream ins;
    ins.open(meta_file.string(), std::fstream::in);
    pt::json_parser::read_json(ins, prev_meta);
    ins.close();

    // Encrypt the password if no salt exists.
    auto pwhash = prev_meta.get_optional<std::string>("password.hash");
    if (pwhash) {
        meta.put("password.hash", *pwhash);
    }
    auto pwsalt = prev_meta.get_optional<std::string>("password.salt");
    if (pwsalt) {
        meta.put("password.salt", *pwsalt);
    }

    std::ofstream ous;
    ous.open(meta_file.string(), std::ostream::out);
    pt::json_parser::write_json(ous, meta);
    ous.close();
}

void data_store::set_password(
    const std::string& username,
    const std::string& password
) {
    const fs::path meta_file = doc_root / "users" / username / "meta.json";

    std::string hash;
    std::string salt;

    if (!fs::exists(meta_file)) {
        return;
    }

    boost::property_tree::ptree meta;
    read_account(username, meta);

    newsalt(salt);
    hash_password(hash, password, salt);

    meta.put("password.hash", hash);
    meta.put("password.salt", salt);

    std::ofstream ous;
    ous.open(meta_file.string(), std::ostream::out);
    pt::json_parser::write_json(ous, meta);
    ous.close();

}

void data_store::list_users(std::vector<std::string>& users) const {
    const fs::path path = doc_root / "users";
    for (
        boost::filesystem::directory_iterator itr = boost::filesystem::directory_iterator(path);
        itr != boost::filesystem::directory_iterator();
        ++itr
    ) {
        users.push_back(itr->path().filename().string());
    }
}

// vim : et sw=4 :
