#include "util.hpp"

#include <string>
#include <vector>
#include <string.h>
#include <openssl/evp.h>

void base64decode(const std::string& in, std::vector<unsigned char>& out) {
    // The output of encode is always divisiable by 4. Padded bytes will be 0.
    out.resize(in.length() / 4 * 3 + 1);

    // Ensure any produced string is terminated.
    memset(&out[0], 0, out.size());

    EVP_DecodeBlock(&out[0], reinterpret_cast<const unsigned char*>(in.c_str()), in.length());
}

void base64encode(const std::vector<unsigned char>& in, std::vector<unsigned char>& out) {
    if (in.size() % 3 == 0) {
        out.resize(in.size() / 3 * 4 + 1);
    } else {
        out.resize(((in.size() / 3) + 1) * 4 + 1);
    }

    EVP_EncodeBlock(&out[0], &in[0], in.size());
}
