#ifndef __DATA_STORE_HPP
#define __DATA_STORE_HPP


#include <string>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>

namespace {
    const boost::property_tree::ptree empty_properties = boost::property_tree::ptree();
}

class data_store_directory_listing {
    std::string flags;
    std::string name;
public:
    data_store_directory_listing(
        const std::string& flags,
        const std::string& name
    );

    std::string string() const;
};

class data_store {

    const boost::filesystem::path doc_root;

public:
    explicit data_store(const std::string& doc_root);

    /**
     * Make an empty account with the given password.
     * 
     * @param[in] username The name of the user.
     * @param[in] password The clear-text password for the user.
     * @param[in] permissions A list of permissions such as "admin" for an administrative user.
     */
    void create_account(
        const std::string& username,
        const std::string& password,
        const std::list<std::string>& permissions
    );

    /**
     * Read in the account meta data, but delete the keys password.hash and password.salt.
     * 
     * @param[in] username The user.
     * @param[out] user The user properties.
     */
    void read_account(
        const std::string& username,
        boost::property_tree::ptree &user
    ) const;
    
    /**
     * Write the user propterties, but perserve the password.hash and password.salt values.
     * 
     * permissions.<role>=1 sets permissions.
     * 
     * This fully replaces the user's properties, so you may want to modify
     * the results of read_account() instead of writing a clean meta.
     * 
     * @param[in] username The user.
     * @param[in] user The user properties.
     */
    void write_account(
        const std::string& username,
        const boost::property_tree::ptree &user
    );
    
    /**
     * Check a password for a user against the stored password hash and salt.
     * 
     * @param[in] username The user name.
     * @param[in] password The clear-text password.
     * @return Return true if the hashed password matches, false otherwise.
     */
    bool check_password(
        const std::string& username,
        const std::string& password
    );

    /**
     * Read a file for a user.
     * 
     * @param[in] username The user.
     * @param[in] filename The name of the file to read.
     * @param[out] out The text of the file read.
     */
    void read_file(
        const std::string& username,
        const std::string& filename,
        std::string& out
    ) const;

    /**
     * Write a file for a user.
     * 
     * @param[in] username The user.
     * @param[in] filename The name of the file to read.
     * @param[in] in The text of the file write.
     * @param[in] properties Extra properties for the file, such as content_type or encryption details like the cipher name.
     */
    void write_file(
        const std::string& username,
        const std::string& filename,
        const std::string& in,
        const boost::property_tree::ptree& properties = empty_properties
    );

    /**
     * Remove a file and its meta data.
     * 
     * @param[in] username The user.
     * @param[in] filename The file to remove.
     */
    void delete_file(
        const std::string& username,
        const std::string& filename
    );

    /**
     * Read the meta data about a file.
     * 
     * @param[in] username The user.
     * @param[in] filename The file.
     * @param[out] properties The meta data.
     */
    void read_file_properties(
        const std::string& username,
        const std::string& filename,
        boost::property_tree::ptree& properties
    ) const;

    /**
     * Write file properties.
     * 
     * @param[in] username The username.
     * @param[in] filename The file to write the properties for.
     * @param[in] properties The properties to write for the named file.
     */
    void write_file_properties(
        const std::string& username,
        const std::string& filename,
        const boost::property_tree::ptree& properties
    );

    /**
     * Does a file exist.
     * 
     * @param[in] username The username.
     * @param[in] filename The file to check.
     * @returns Return true if the file exists, false otherwise.
     */
    bool exists(
        const std::string& username,
        const std::string& filename
    ) const;

    /**
     * Return a list of file directory objects.
     * 
     * @param[in] username The user.
     * @param[in] dirname The directory to list.
     * @param[out] listing A list of directory entries.
     */
    void list(
        const std::string& username,
        const std::string& dirname,
        std::vector<data_store_directory_listing>& listing
    ) const;

    /**
     * Is the file a directory.
     * 
     * @param[in] username The username.
     * @param[in] path The path to check.
     * @returns Returns true if the file is a directory, false otherwise.
     */
    bool is_directory(
        const std::string& username,
        const std::string& path
    ) const;

    /**
     * @param[in] role The role to check for.
     * @param[in] properties Properties such as those fetched using read_account().
     * @returns Returns true if the given user properties contains the key permissions.<role>.
     * @see read_account()
     */
    bool has_role(
        const std::string& role,
        const boost::property_tree::ptree& properties
    ) const;

    /**
     * Generate a new hash and salt of a given user password.
     * 
     * @param[in] username The user.
     * @param[in] password The clear text password to hash.
     */
    void set_password(
        const std::string& username,
        const std::string& password
    );

    /**
     * Put the users into the given vector.
     *
     *  @param[out] users Put the users in this output vector.
     */
    void list_users(std::vector<std::string>& users) const;

private:

};

#endif

// vim : et sw=4 :
