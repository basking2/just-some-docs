
// vim: set et sw=4 :

#ifndef __HTTP_HPP
#define __HTTP_HPP

#include <string>

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/strand.hpp>

#include <cstdlib>
#include <thread>

#include "http_listener.hpp"
#include "just_some_docs.hpp"

class HttpServer {
    private:
        std::string cert;
        std::string key;
    public:
        HttpServer(const std::string& cert, const std::string& key, just_some_docs& app): cert(cert), key(key) {

            boost::system::error_code ec;

            auto const address = boost::asio::ip::make_address("0.0.0.0");
            auto const port = static_cast<unsigned short>(std::atoi("8088"));
            auto const doc_root = std::make_shared<std::string>("./node/dist");
            auto const threads = std::max<int>(1, std::atoi("12"));

            boost::asio::io_context ioc{threads};
            //boost::asio::ssl::context ctx{boost::asio::ssl::context::tlsv13};
            boost::asio::ssl::context ctx{boost::asio::ssl::context::tls};
            ctx.use_certificate_file(cert, boost::asio::ssl::context::file_format::pem, ec);
            if (ec.failed()) {
                throw std::runtime_error("Failure, cert.");
            }

            ctx.use_private_key_file(key, boost::asio::ssl::context::file_format::pem, ec);
            if (ec.failed()) {
                throw std::runtime_error("Failure, key.");
            }

            auto lst = std::make_shared<listener>(ioc, ctx, boost::asio::ip::tcp::endpoint{address, port}, doc_root, app, ec);
            if (ec) {
                throw std::runtime_error("Failed to initialize the listener.");
            }

            lst->run();

            // Run the I/O service on the requested number of threads
            std::vector<std::thread> v;
            v.reserve(threads - 1);
            for(auto i = threads - 1; i > 0; --i)
                v.emplace_back(
                [&ioc]
                {
                    ioc.run();
                });
            ioc.run();
        }
};

#endif
