module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  'devServer': {
    'proxy': {
      '^/api': {
        'target': 'https://127.0.0.1:8088',
        'secure': false,
        // Do not auth as this is part of the server.
        // 'auth': 'admin:admin'
      }
    }
  },
  configureWebpack: {
    plugins: [
      
    ]
  }
}
