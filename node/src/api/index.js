
const utilEvent = require('../util/event')
const http = require('http')

const basePath = '/api'

/**
 * Call http.request() with the given options.
 * 
 * Some response are handled differently such as a 401 response triggering
 * an event being thrown so the upper-level of the application can 
 * respond to it.
 * 
 * @param {object} opts 
 * @param {function} cb Callback that receives the response object.
 */
function request(vueComponent, opts, cb) {
    if (!('headers' in opts)) {
        opts.headers = {}
    }

    opts.headers['X-JustSomeDocs-UI'] = 1

    return http.request(opts, res => {
        if (res.statusCode == 401 && vueComponent) {
            utilEvent.dispatchAuthError(vueComponent)
        } else {
            cb(res)
        }
    })
}

function readProps(opts, callback) {

    var vueComponent = opts.vueComponent
    var user = opts.user
    var pass = opts.pass
    var file = opts.file

    var req = request(
        vueComponent, 
        {
            // ... window.location,

            method: 'GET',
            path: `${basePath}/${file}?props=true`,
            auth: `${user}:${pass}`
        },
        res => {
            if (res.statusCode == 200) {
                var data = ''
                res.on('data', chunk => data += chunk)
                res.on('end', () => {
                    try {
                        var json = JSON.parse(data) || {}
                        callback(undefined, json)
                    }
                    catch(e) {
                        callback(e, {})
                    }
                })
                res.on('error', err => callback(err, {}))
            } else {
                callback(new Error(`Status code is ${res.statusCode}`), {})
            }
        }
    )

    // No body.
    req.end()
}

function readFile(opts, callback) {

    var vueComponent = opts.vueComponent
    var user = opts.user
    var pass = opts.pass
    var file = opts.file

    var req = request(
        vueComponent, 
        {
            // ... window.location,

            method: 'GET',
            path: `${basePath}/${file}`,
            auth: `${user}:${pass}`
        },
        res => {
            if (res.statusCode == 200) {
                var data = ''
                res.on('data', chunk => data += chunk)
                res.on('end', () => callback(undefined, data))
                res.on('error', err => callback(err, data))
            } else {
                callback(new Error(`Status code is ${res.statusCode}.`), '')
            }
        }
    )

    // No body.
    req.end()
}

function writeFile(opts, callback) {

    var vueComponent = opts.vueComponent
    var user = opts.user
    var pass = opts.pass
    var file = opts.file
    var props = opts.props || {}

    var reqOpts = {
        method: 'PUT',
        path: `${basePath}/${file}`,
        auth: `${user}:${pass}`,
        headers: {}
    }

    reqOpts.headers['X-JustSomeDocs-Props'] = []

    for (var k in props) {
        if (k != 'body') {
            reqOpts.headers['X-JustSomeDocs-Props'].push(`${k}=${props[k]}`)
        }
    }

    var req = request(vueComponent, reqOpts, res => {
        var data = ''
        res.on('data', chunk => data += chunk)
        res.on('end', () => callback(undefined, data))
        res.on('error', err => callback(err, data))
    })

    req.end(props['body'])
}

function listDir(opts, callback) {
    var vueComponent = opts.vueComponent
    var user = opts.user
    var pass = opts.pass
    var path = opts.path

    if (!path.endsWith('/')) {
        path += '/'
    }

    var req = request(
        vueComponent, 
        {
            // ... window.location,

            method: 'GET',
            path: `${basePath}/${path}`,
            auth: `${user}:${pass}`
        },
        res => {
            var data = ''
            res.on('data', chunk => data += chunk)
            res.on('end', () => {
                var rows = data.split("\n").
                    map(row => row.split(" ", 2)).
                    filter(row => row[1])

                callback(undefined, rows)
            })
            res.on('error', err => callback(err, []))
        }
    )

    // No body.
    req.end()
}

function readAccount(opts, callback) {
    var vueComponent = opts.vueComponent
    var user = opts.user
    var pass = opts.pass
    var account = opts.account

    var reqOpts = {
        method: 'GET',
        path: `${basePath}/${account}`,
    }

    if ('user' in opts && 'pass' in opts) {
        reqOpts['auth'] = `${user}:${pass}`
    }

    var req = request(
        vueComponent,
        reqOpts,
        res => {
            if (res.statusCode == 200) {
                var data = ''
                res.on('data', chunk => data += chunk)
                res.on('end', () => {
                    callback(undefined, JSON.parse(data))
                })
                res.on('error', err => callback(err, []))
            } else {
                callback(new Error(`Status code is ${res.statusCode}.`))
            }
        }
    )

    // No body.
    req.end()    
}

function listUsers(opts, callback) {
    var vueComponent = opts.vueComponent
    var user = opts.user
    var pass = opts.pass

    var reqOpts = {
        method: 'GET',
        path: `${basePath}`,
    }

    if ('user' in opts && 'pass' in opts) {
        reqOpts['auth'] = `${user}:${pass}`
    }

    var req = request(
        vueComponent,
        reqOpts,
        res => {
            var data = ''
            res.on('data', chunk => data += chunk)
            res.on('end', () => {
                var rows = data.split("\n").filter(arr => arr)

                callback(undefined, rows)
            })
            res.on('error', err => callback(err, []))
        }
    )

    // No body.
    req.end()
}

function setPassword(opts, callback) {
    var vueComponent = opts.vueComponent
    var user = opts.user
    var pass = opts.pass
    var newpass = opts.newpass
    var account = opts.account || user

    var reqOpts = {
        method: 'POST',
        path: `${basePath}/${account}`,
        auth: `${user}:${pass}`
    }

    var req = request(
        vueComponent,
        reqOpts,
        res => {
            var data = ''
            res.on('data', chunk => data += chunk)
            res.on('end', () => callback(undefined, data))
            res.on('error', err => callback(err, data))
        }
    )

    req.end(JSON.stringify({
        password: {
            plain: newpass
        }
    }))
}

function createUser(opts, callback) {
    var vueComponent = opts.vueComponent
    var user = opts.user
    var pass = opts.pass
    var accountName = opts.accountName
    var account = opts.account

    var reqOpts = {
        method: 'PUT',
        path: `${basePath}/${accountName}`,
        auth: `${user}:${pass}`
    }

    var req = request(
        vueComponent,
        reqOpts,
        res => {
            var data = ''
            res.on('data', chunk => data += chunk)
            res.on('end', () => callback(undefined, data))
            res.on('error', err => callback(err, data))
        }
    )

    req.end(JSON.stringify(account))
}

module.exports = {
    request,
    readFile,
    readProps,    
    writeFile,
    listDir,
    listUsers,
    readAccount,
    setPassword,
    createUser
}