import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import themes from './vuetify_themes';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: themes.light,
            // dark: themes.dark
        }
    }
});
