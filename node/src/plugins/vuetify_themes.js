export const light = {
    primary: '#3f51b5',
    secondary: '#607d8b',
    accent: '#ffc107',
    error: '#f44336',
    warning: '#ffeb3b',
    info: '#4caf50',
    success: '#8bc34a'
}

export const dark = {}

export default {
    light, dark
}