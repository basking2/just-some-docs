/**
 * Just Some Docs library routines.
 * 
 * This is mostly the encryption logic.
 */

 const encryption = require('./encryption')

 export const encrypt = encryption.encrypt
 export const decrypt = encryption.decrypt

 export default {
     encrypt: encryption.encrypt,
     decrypt: encryption.decrypt,
 }