import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'

Vue.use(Vuex)
Vue.config.productionTip = false

const store = new Vuex.Store({
  state: {
    login: {
      user: undefined,
      pass: undefined,
      docpass: undefined,
    }
  },
  mutations: {
    clearCredentials(state) {
      state.login = {}
      window.localStorage.removeItem('jsdLogin')
    },
    credentials(state, creds){
      var o
      var user = creds.user
      var pass= creds.pass
      var docpass = creds.docpass
            
      try {
          o = JSON.parse(window.localStorage.getItem('jsdLogin'))
      } catch {
          o = {}
      }

      if (!o) {
          o = {}
      }

      if (user) {
          state.login.user = user
          o['user'] = user
      }
      
      if (pass) {
          state.login.pass = pass
          o['pass'] = pass
      }

      if (docpass) {
        state.login.pass = docpass
        o['docpass'] = docpass
      }

      window.localStorage.setItem('jsdLogin', JSON.stringify(o))
    }
  },
  getters: {
    credentials: (state) => {

      var o = {}

      if (state.login.user || state.login.pass) {
        o = state.login
      }

      o = JSON.parse(window.localStorage.getItem('jsdLogin') || '{}')

      return o

    }
  }
})

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
