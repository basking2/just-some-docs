import os
import re

def darwin_brew_add_openssl(env):
    basedir = '/usr/local/Cellar'

    d = sorted([ d for d in os.listdir(basedir) if d.startswith('openssl@') ])
    if len(d) == 0:
        raise Exception("OpenSSL not found.")

    basedir = os.path.join(basedir, d[0])
        
    d = sorted([d for d in os.listdir(basedir) ])
    if len(d) == 0:
        raise Exception("OpenSSL not found.")

    openssl_prefix = os.path.join(basedir, d[0])

    env.MergeFlags([f'-I{openssl_prefix}/include', f'-L{openssl_prefix}/lib', '-lssl', '-lcrypto'])


def add_openssl(env):
    platform = env['PLATFORM']
    
    if (platform == 'darwin'):
        darwin_brew_add_openssl(env)
    
    try:
        env.ParseConfig("pkg-config openssl --cflags --libs")
    except OSError:
        # Fail back to macOS.
        darwin_brew_add_openssl(env)
