#define BOOST_TEST_MODULE BasicTests
#include <boost/test/unit_test.hpp>

#include "data_store.hpp"

#include <list>
#include <string>
//#include <openssl/ssl.h>

BOOST_AUTO_TEST_CASE( constructors_test )
{
    data_store d("build/tests/data_store");

    std::list<std::string> permissions;

    permissions.push_back("admin");
    permissions.push_back("test_user");

    d.create_account("user1", "user1", permissions);
    BOOST_REQUIRE(!d.check_password("no_user", "user1"));
    BOOST_REQUIRE(!d.check_password("user1", "wrong_password"));
    BOOST_REQUIRE(d.check_password("user1", "user1"));
    BOOST_CHECK_EQUAL('t', 't');
}

BOOST_AUTO_TEST_CASE( read_write_files )
{
    data_store d("build/tests/data_store");

    std::list<std::string> permissions;

    permissions.push_back("admin");
    permissions.push_back("test_user");

    d.create_account("user1", "user1", permissions);

    d.write_file("user1", "a/b/c", "data");

    BOOST_REQUIRE(d.exists("user1", "a/b/c"));
    BOOST_REQUIRE(!d.exists("user1", "a/b/not_here"));

    std::string data;
    d.read_file("user1", "a/b/c", data);

    BOOST_CHECK_EQUAL(data, "data");

    d.delete_file("user1", "a/b/c");

    BOOST_REQUIRE(!d.exists("user1", "a/b/c"));
    BOOST_REQUIRE(!d.exists("user1", "a/b"));
    BOOST_REQUIRE(!d.exists("user1", "a"));
    BOOST_REQUIRE(d.exists("user1", "."));
}

BOOST_AUTO_TEST_CASE( properties_manipulation )
{
    data_store d("build/tests/data_store");
    boost::property_tree::ptree pt;

    d.set_password("admin", "adminpw");
    BOOST_CHECK_EQUAL(false, d.check_password("admin", "admin"));
    BOOST_REQUIRE(d.check_password("admin", "adminpw"));

    pt.clear();
    d.read_account("admin", pt);
    BOOST_REQUIRE(d.has_role("admin", pt));
    BOOST_REQUIRE(!d.has_role("nothing", pt));
    BOOST_REQUIRE(!pt.get_optional<std::string>("password.salt"));
    BOOST_REQUIRE(!pt.get_optional<std::string>("password.hash"));

    // Put the old password back.
    d.set_password("admin", "admin");
    BOOST_REQUIRE(d.check_password("admin", "admin"));

    pt.clear();
    d.read_account("admin", pt);
    pt.put("a.b.c.d", "test value");

    d.write_account("admin", pt);
    BOOST_REQUIRE(d.check_password("admin", "admin"));

    pt.clear();
    d.read_account("admin", pt);
    BOOST_CHECK_EQUAL("test value", pt.get<std::string>("a.b.c.d"));

    pt.clear();
    d.read_account("admin", pt);
    pt.erase("a");
    d.write_account("admin", pt);
    pt.clear();
    d.read_account("admin", pt);
    BOOST_REQUIRE(!pt.get_child_optional("a.b.c.d"));
}
